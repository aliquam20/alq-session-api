package org.aliquam.session.api.model;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class SessionTest {

    @Test
    void hasExpired() {
        LocalDateTime now = LocalDateTime.now();
        Session session = new Session(null, null, null, now);
        assertThat(session.hasExpired(now.minus(Duration.ofSeconds(1))), is(false));
        assertThat(session.hasExpired(now), is(false));
        assertThat(session.hasExpired(now.plus(Duration.ofSeconds(1))), is(true));
    }
}