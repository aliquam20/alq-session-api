package org.aliquam.session.api.connector;

import org.aliquam.session.api.model.CreateSessionRequest;
import org.aliquam.session.api.model.Session;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class AlqSessionManagerImplTest {

    private AlqSessionConnector getConnector() {
        return new AlqSessionConnector(null) {
            @Override
            public Session createSession(CreateSessionRequest createSessionRequest) {
                return new Session(UUID.randomUUID(), null, null, LocalDateTime.now());
            }

            @Override
            public Session extendSession(UUID id) {
                return new Session(UUID.randomUUID(), null, null, LocalDateTime.now());
            }
        };
    }

    @Test
    void shouldCreateSessionOnFirstCall() {
        LocalDateTime now = LocalDateTime.now();
        Session SESSION = new Session(UUID.fromString("00000000-955f-480e-a8a9-798cb4c2abfa"), null, null, now);

        AlqSessionConnector connector = mock(AlqSessionConnector.class);
        when(connector.createSession(any())).thenReturn(SESSION);

        AlqSessionManagerImpl manager = new AlqSessionManagerImpl(UUID.randomUUID(), "", connector);
        Session managerSession = manager.getSession();
        assertThat(managerSession.getId(), is(SESSION.getId()));
        verify(connector, times(1)).createSession(any());
        Mockito.verifyNoMoreInteractions(connector);
    }

    @Test
    void shouldReturnSameSessionIfStillValid() {
        Session SESSION = new Session(UUID.fromString("00000000-955f-480e-a8a9-798cb4c2abfa"), null, null,
                LocalDateTime.now().plus(Duration.ofDays(30)));

        AlqSessionConnector connector = mock(AlqSessionConnector.class);
        when(connector.createSession(any())).thenReturn(SESSION);

        AlqSessionManagerImpl manager = new AlqSessionManagerImpl(UUID.randomUUID(), "", connector);
        Session managerSession1 = manager.getSession();
        Session managerSession2 = manager.getSession();

        assertThat(managerSession1.getId(), is(SESSION.getId()));
        assertThat(managerSession1.getId(), is(managerSession2.getId()));
        verify(connector, times(1)).createSession(any());
        Mockito.verifyNoMoreInteractions(connector);
    }

    @Test
    void shouldCreateSessionIfExpired() {
        LocalDateTime now = LocalDateTime.now();
        Session EXPIRED_SESSION = new Session(UUID.fromString("00000000-955f-480e-a8a9-798cb4c2abfa"), null, null,
                LocalDateTime.now().minus(Duration.ofDays(1)));
        Session SESSION = new Session(UUID.fromString("00000001-955f-480e-a8a9-798cb4c2abfa"), null, null,
                LocalDateTime.now().plus(Duration.ofDays(1)));

        AlqSessionConnector connector = mock(AlqSessionConnector.class);
        when(connector.createSession(any()))
                .thenReturn(EXPIRED_SESSION)
                .thenReturn(SESSION);

        AlqSessionManagerImpl manager = new AlqSessionManagerImpl(UUID.randomUUID(), "", connector);

        Session managerSession1 = manager.getSession();
        assertThat(managerSession1.getId(), is(EXPIRED_SESSION.getId()));

        Session managerSession2 = manager.getSession();
        assertThat(managerSession2.getId(), is(SESSION.getId()));

        verify(connector, times(2)).createSession(any());
        Mockito.verifyNoMoreInteractions(connector);
    }

    @Test
    void shouldCreateSessionIfCloseToExpiration() {
        LocalDateTime now = LocalDateTime.now();
        Session ALMOST_EXPIRED_SESSION = new Session(UUID.fromString("00000000-955f-480e-a8a9-798cb4c2abfa"), null, null,
                LocalDateTime.now().plus(Duration.ofMinutes(1)));
        Session SESSION = new Session(UUID.fromString("00000001-955f-480e-a8a9-798cb4c2abfa"), null, null,
                LocalDateTime.now().plus(Duration.ofDays(1)));

        AlqSessionConnector connector = mock(AlqSessionConnector.class);
        when(connector.createSession(any()))
                .thenReturn(ALMOST_EXPIRED_SESSION)
                .thenReturn(SESSION);

        AlqSessionManagerImpl manager = new AlqSessionManagerImpl(UUID.randomUUID(), "", connector);

        Session managerSession1 = manager.getSession();
        assertThat(managerSession1.getId(), is(ALMOST_EXPIRED_SESSION.getId()));

        Session managerSession2 = manager.getSession();
        assertThat(managerSession2.getId(), is(SESSION.getId()));

        verify(connector, times(2)).createSession(any());
        Mockito.verifyNoMoreInteractions(connector);
    }


    @Test
    void shouldExtendSessionIfWithinExpirationPeriod() {
        LocalDateTime now = LocalDateTime.now();
        Session ALMOST_EXPIRED_SESSION = new Session(UUID.fromString("00000000-955f-480e-a8a9-798cb4c2abfa"), null, null,
                LocalDateTime.now().plus(Duration.ofMinutes(30)));
        Session SESSION = new Session(UUID.fromString("00000000-955f-480e-a8a9-798cb4c2abfa"), null, null,
                LocalDateTime.now().plus(Duration.ofDays(1)));

        AlqSessionConnector connector = mock(AlqSessionConnector.class);
        when(connector.createSession(any())).thenReturn(ALMOST_EXPIRED_SESSION);
        when(connector.extendSession(ALMOST_EXPIRED_SESSION.getId())).thenReturn(SESSION);


        AlqSessionManagerImpl manager = new AlqSessionManagerImpl(UUID.randomUUID(), "", connector);

        Session managerSession1 = manager.getSession();
        assertThat(managerSession1.getId(), is(ALMOST_EXPIRED_SESSION.getId()));

        Session managerSession2 = manager.getSession();
        assertThat(managerSession2.getId(), is(SESSION.getId()));

        verify(connector, times(1)).createSession(any());
        verify(connector, times(1)).extendSession(ALMOST_EXPIRED_SESSION.getId());
        Mockito.verifyNoMoreInteractions(connector);
    }


}