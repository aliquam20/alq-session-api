package org.aliquam.session.api;

import org.aliquam.session.api.model.Contract;
import org.aliquam.session.api.model.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

class RequestAuthenticatorServiceTest {

    @Test
    void checkSession_works() throws RequestAuthenticationException {
        final LocalDateTime now = LocalDateTime.of(2020, 6, 1, 12, 0, 0);
        final Session session = new Session(null, new Contract(null, null, false), UUID.randomUUID(), now);
        final RequestAuthenticatorService service = new RequestAuthenticatorService((uuid) -> Optional.of(session));

        service.checkSession(UUID.randomUUID().toString(), false, now);
    }

    @Test
    void checkSession_throwsWhenSessionHeaderMissing() {
        final RequestAuthenticatorService service = new RequestAuthenticatorService(null);
        RequestAuthenticationException ex = Assertions.assertThrows(RequestAuthenticationException.class, () -> {
            service.checkSession(null, false, null);
        });
        assertThat(ex.getMessage(), containsString("Session header is missing"));
    }

    @Test
    void checkSession_throwsWhenSessionHeaderInvalid() {
        final RequestAuthenticatorService service = new RequestAuthenticatorService(null);
        RequestAuthenticationException ex = Assertions.assertThrows(RequestAuthenticationException.class, () -> {
            service.checkSession("aaaa", false, null);
        });
        assertThat(ex.getMessage(), containsString("Invalid session header format"));
    }

    @Test
    void checkSession_throwsWhenSessionNotFound() {
        final RequestAuthenticatorService service = new RequestAuthenticatorService((uuid) -> Optional.empty());

        RequestAuthenticationException ex = Assertions.assertThrows(RequestAuthenticationException.class, () -> {
            service.checkSession(UUID.randomUUID().toString(), false, null);
        });
        assertThat(ex.getMessage(), containsString("Session not found"));
    }

    @Test
    void checkSession_throwsWhenSessionExpired() {
        final LocalDateTime now = LocalDateTime.of(2020, 6, 1, 12, 0, 0);
        final Session session = new Session(null, null, null, now.minus(Duration.ofSeconds(1)));
        final RequestAuthenticatorService service = new RequestAuthenticatorService((uuid) -> Optional.of(session));

        RequestAuthenticationException ex = Assertions.assertThrows(RequestAuthenticationException.class, () -> {
            service.checkSession(UUID.randomUUID().toString(), false, now);
        });
        assertThat(ex.getMessage(), containsString("Session has expired"));
    }

    @Test
    void checkSession_throwsWhenSessionIsNotInternalWhenRequired() {
        final LocalDateTime now = LocalDateTime.of(2020, 6, 1, 12, 0, 0);
        final Session session = new Session(null, new Contract(null, null, false), UUID.randomUUID(), now);
        final RequestAuthenticatorService service = new RequestAuthenticatorService((uuid) -> Optional.of(session));

        RequestAuthenticationException ex = Assertions.assertThrows(RequestAuthenticationException.class, () -> {
            service.checkSession(UUID.randomUUID().toString(), true, now);
        });
        assertThat(ex.getMessage(), containsString("This endpoint requires internal contract"));
    }
}