package org.aliquam.session.api;

import org.aliquam.session.api.connector.AlqSessionManager;
import org.aliquam.session.api.connector.AlqSessionManagerImpl;

import java.util.UUID;

public class AlqSessionApi {
    public static final String SESSION_HEADER = "x-alq-session";

    public static AlqSessionManager provideAlqSessionManager() {
        String contract = System.getenv("ALQ_COMM_CONTRACT_ID");
        String password = System.getenv("ALQ_COMM_CONTRACT_PASS");
        if(contract == null || password == null) {
            throw new IllegalStateException("Expected contract and password to be set in environment variables");
        }
        return new AlqSessionManagerImpl(UUID.fromString(contract), password);
    }
}
