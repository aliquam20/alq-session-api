package org.aliquam.session.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateContractRequest {

    public CreateContractRequest(Contract contract, String password) {
        this(contract.getId(), contract.getName(), contract.isInternal(), password);
    }

    @NotNull
    private UUID id;

    @NotBlank
    private String name;

    private boolean internal;

    private String password;
}
