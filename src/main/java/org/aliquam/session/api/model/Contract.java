package org.aliquam.session.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Contract {
    @NotNull
    private UUID id;

    @NotBlank
    private String name;

    private boolean internal;

    public Contract(String name, boolean internal) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.internal = internal;
    }
}