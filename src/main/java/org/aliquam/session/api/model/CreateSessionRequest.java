package org.aliquam.session.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateSessionRequest {
    @NotNull
    private UUID contract;

    @ToString.Exclude
    private String password;

    private UUID tenant;
}
