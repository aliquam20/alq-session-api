package org.aliquam.session.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Session {
    @NotNull
    private UUID id;

    @NotNull
    private Contract contract;

    private UUID tenant;

    @NotNull
    private LocalDateTime expires;

    public boolean hasExpired(LocalDateTime time) {
        return time.isAfter(expires);
    }
}
