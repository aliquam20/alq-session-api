package org.aliquam.session.api;

import lombok.Getter;

public class RequestAuthenticationException extends Exception {

    @Getter
    private final Integer errorHttpStatus;

    public RequestAuthenticationException(Integer errorHttpStatus, String errorDetails) {
        super(errorDetails);
        this.errorHttpStatus = errorHttpStatus;
    }
}
