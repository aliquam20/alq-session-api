package org.aliquam.session.api.connector;

import com.fasterxml.jackson.core.type.TypeReference;
import org.aliquam.cum.network.AlqConnector;
import org.aliquam.cum.network.AlqRequestBuilder;
import org.aliquam.cum.network.AlqUrl;
import org.aliquam.cum.network.exception.client.AlqNotFoundResponseException;
import org.aliquam.session.api.model.Contract;
import org.aliquam.session.api.model.CreateContractRequest;
import org.aliquam.session.api.model.CreateSessionRequest;
import org.aliquam.session.api.model.Session;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class AlqSessionConnector {
    private static final AlqUrl alqSessionService = AlqConnector.of("session");
    private static final AlqUrl sessionResource = alqSessionService.resolve("session");
    private static final AlqUrl contractResource = alqSessionService.resolve("contract");

    private final AlqSessionManager sessionManager;

    public AlqSessionConnector(AlqSessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public Contract putContract(UUID id, String name, boolean internal, String password) {
        return putContract(new CreateContractRequest(id, name, internal, password));
    }

    public Contract putContract(CreateContractRequest createContractRequest) {
        return new AlqRequestBuilder(contractResource)
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .PUT(createContractRequest)
                .build().execute(Contract.class);
    }

    public List<Contract> listContracts() {
        // TODO: Czy to nizej jest wciaz aktualne?
        // Convert2Diamond marks "new GenericType<List<Contract>>". Though simplifying that makes compiler crash.
        //noinspection Convert2Diamond
        return new AlqRequestBuilder(contractResource)
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(new TypeReference<List<Contract>>() {});
    }

    public void deleteContract(Contract contract) {
        deleteContract(contract.getId());
    }

    public void deleteContract(UUID id) {
        new AlqRequestBuilder(contractResource.resolve(id))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .DELETE()
                .build().execute();
    }

    public Session createSession(UUID contract, String password, UUID tenant) {
        return createSession(new CreateSessionRequest(contract, password, tenant));
    }

    public Session createSession(CreateSessionRequest createSessionRequest) {
        return new AlqRequestBuilder(sessionResource)
                .acceptApplicationJson()
                .withRetries() // In worst case it'll create some unnecessary sessions that will be unused
                .POST(createSessionRequest)
                .build().execute(Session.class);
    }

    public Session getSession(UUID id) {
        return new AlqRequestBuilder(sessionResource.resolve(id))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(Session.class);
    }

    public Optional<Session> getSessionOrEmpty(UUID id) {
        try {
            return Optional.of(getSession(id));
        } catch (AlqNotFoundResponseException ex) {
            return Optional.empty();
        }
    }

    public Session extendSession(Session session) {
        return extendSession(session.getId());
    }

    public Session extendSession(UUID id) {
        return new AlqRequestBuilder(sessionResource.resolve(id).resolve("extend"))
                .acceptApplicationJson()
                .withRetries()
                .POST(null)
                .build().execute(Session.class);
    }

}
