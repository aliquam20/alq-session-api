package org.aliquam.session.api.connector;

import org.aliquam.session.api.model.Session;
import org.aliquam.session.api.model.CreateSessionRequest;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;

public class AlqSessionManagerImpl implements AlqSessionManager {
    private final AlqSessionConnector connector;
    private final UUID contract;
    private final String contractPassword;
    private Session session = new Session(new UUID(0, 0), null, null, LocalDateTime.MIN);

    public AlqSessionManagerImpl(UUID contract, String contractPassword) {
        this(contract, contractPassword, new AlqSessionConnector(null));
    }

    public AlqSessionManagerImpl(UUID contract, String contractPassword, AlqSessionConnector connector) {
        this.contract = contract;
        this.contractPassword = contractPassword;
        this.connector = connector;
    }

    @Override
    public Session getSession() {
        refreshSessionIfNeeded();
        return session;
    }

    private void refreshSessionIfNeeded() {
        boolean expiredOrCloseToExpiration = session.hasExpired(LocalDateTime.now().plus(Duration.ofMinutes(5)));
        if(expiredOrCloseToExpiration) {
            // Drop; Using such sessions poses a risk that session is valid when sending
            //     but due to network latency it's expired when arriving at the server.
            // Also minor clock desynchronization is another risk.
            CreateSessionRequest request = new CreateSessionRequest(contract, contractPassword, null);
            session = connector.createSession(request);
            return;
        }

        boolean withinExtensionPeriod = session.hasExpired(LocalDateTime.now().plus(Duration.ofDays(3)));
        if(withinExtensionPeriod) {
            session = connector.extendSession(session.getId());
        }
    }

}
