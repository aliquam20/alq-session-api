package org.aliquam.session.api.connector;

import org.aliquam.session.api.model.Session;

import java.time.LocalDateTime;
import java.util.UUID;

public class AlqDummySessionManager implements AlqSessionManager {

    @Override
    public Session getSession() {
        return new Session(new UUID(0, 0), null, null, LocalDateTime.MIN);
    }

}
