package org.aliquam.session.api.connector;//package org.aliquam.config.api.connector;

import org.aliquam.session.api.model.Session;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class CachedAlqSessionConnector {

    private final ConcurrentHashMap<UUID, Session> cache = new ConcurrentHashMap<>();
    private final AlqSessionConnector connector;

    public CachedAlqSessionConnector(AlqSessionConnector connector) {
        this.connector = connector;
    }

    public void clear() {
        // TODO: clear on queue message
        cache.clear();
    }

    public Session getSession(UUID id) {
        return cache.computeIfAbsent(id, kkk -> connector.getSession(id));
    }
}
