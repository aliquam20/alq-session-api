package org.aliquam.session.api;

import lombok.extern.slf4j.Slf4j;
import org.aliquam.session.api.model.Session;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

@Slf4j
public class RequestAuthenticatorService {
    private static final int UNAUTHORIZED = 401;
    private static final int FORBIDDEN = 403;

    private final Function<UUID, Optional<Session>> sessionRepositoryById;

    public RequestAuthenticatorService(Function<UUID, Optional<Session>> sessionRepositoryById) {
        this.sessionRepositoryById = sessionRepositoryById;
    }

    public void checkSession(String sessionHeader, boolean requireInternal) throws RequestAuthenticationException {
        checkSession(sessionHeader, requireInternal, LocalDateTime.now());
    }

    public void checkSession(String sessionHeader, boolean requireInternal, LocalDateTime now) throws RequestAuthenticationException {
        log.info("Checking session: {}", sessionHeader);

        if(sessionHeader == null) {
            log.info("Session header is missing");
            throw new RequestAuthenticationException(UNAUTHORIZED, "Session header is missing");
        }

        UUID sessionId;
        try {
            sessionId = UUID.fromString(sessionHeader);
        } catch (IllegalArgumentException ex) {
            log.info("Invalid session header format: {}", sessionHeader);
            throw new RequestAuthenticationException(UNAUTHORIZED, "Invalid session header format: " + sessionHeader);
        }

        Session session = sessionRepositoryById.apply(sessionId)
                .orElseThrow(() -> {
                    log.info("Session not found; id: {}", sessionId);
                    return new RequestAuthenticationException(UNAUTHORIZED, "Session not found; id: " + sessionId);
                });

        if(session.hasExpired(now)) {
            log.info("Session has expired; Expires: {}", session.getExpires());
            throw new RequestAuthenticationException(UNAUTHORIZED, "Session has expired; expires: " + session.getExpires());
        }

        if(requireInternal && !session.getContract().isInternal()) {
            log.info("This endpoint requires internal contract");
            throw new RequestAuthenticationException(FORBIDDEN, "This endpoint requires internal contract");
        }

        log.info("Session OK");

    }

}
